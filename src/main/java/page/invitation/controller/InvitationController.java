package page.invitation.controller;


import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import page.invitation.entity.Invitation;
import page.invitation.mapper.InvitationMapper;


import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@RestController
@RequestMapping("invitation")
public class InvitationController {

    @Autowired
    private InvitationMapper invitationMapper;

    //增(id)
    @SneakyThrows
    @RequestMapping("/insertInvitation")
    public String insertInvitation(String user){
        Invitation invitation = new Invitation();
        invitation.setUser(user);
        invitationMapper.insert(invitation);
        return "插入成功";
    }

    //删(id)
    @SneakyThrows
    @RequestMapping("/delInvitation")
    public String delInvitation(Integer id){
        invitationMapper.deleteById(id);
        return "删除成功";
    }

    //改(id)
    @SneakyThrows
    @RequestMapping("/updateInvitation")
    public String updateInvitation(Integer id, String user){
        Invitation invitation = new Invitation();
        invitation.setId(id);
        invitation.setUser(user);
        invitationMapper.updateById(invitation);
        return "修改成功";
    }

    //查（all）
    @SneakyThrows
    @RequestMapping("/getInvitation")
    public String getInvitation(){
        List<Invitation> list = invitationMapper.selectList(null);
//        list.forEach(item->log.info(String.valueOf(item)));
        list.forEach(System.out::println);
        System.out.println(list);
        return "查询成功" + list;
    }

}
