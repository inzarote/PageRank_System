package page.invitation.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Getter
@Setter
@Data
@TableName("invitation")
@ApiModel(value = "Invitation对象", description = "")
public class Invitation implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Integer id;

    @TableField("user")
    private String user;

    @TableField("reply_n")
    private String reply_n;

    @TableField("time")
    private String time;


}
