package page.invitation.mapper;

import page.invitation.entity.Invitation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Mapper
public interface InvitationMapper extends BaseMapper<Invitation> {

}
