package page.invitation.service;

import page.invitation.entity.Invitation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
public interface InvitationService extends IService<Invitation> {

}
