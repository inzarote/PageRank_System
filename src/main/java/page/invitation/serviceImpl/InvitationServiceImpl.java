package page.invitation.serviceImpl;

import page.invitation.entity.Invitation;
import page.invitation.mapper.InvitationMapper;
import page.invitation.service.InvitationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Service
public class InvitationServiceImpl extends ServiceImpl<InvitationMapper, Invitation> implements InvitationService {

}
