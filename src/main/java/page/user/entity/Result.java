package page.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Getter
@Setter
@Data
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;

    private String error;

    private Integer id;

    private String Name;

    private String password;

    private String phone;

}
