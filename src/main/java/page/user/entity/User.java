package page.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Getter
@Setter
@Data
@TableName("user")
@ApiModel(value = "User对象", description = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField("name")
    private String name;

    @TableField("sex")
    private String sex;

    @TableField("birth")
    private String birth;

    @TableField("fans")
    private String fans;

    @TableField("follows")
    private String follows;

    @TableField("manor_n")
    private String manor_n;

    @TableField("invitation_n")
    private String invitation_n;

    @TableField("password")
    private String password;
}
