package page.user.mapper;

import page.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
