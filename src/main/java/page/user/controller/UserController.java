package page.user.controller;


import com.mysql.cj.xdevapi.JsonArray;
import com.mysql.cj.xdevapi.JsonString;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import page.user.entity.Result;
import page.user.entity.User;
import page.user.mapper.UserMapper;

import java.time.LocalDateTime;
import java.util.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    //增(id)
    @SneakyThrows
    @RequestMapping("/insertUser")
    public String insertUser(String name){
        User user = new User();
        user.setName(name);
        userMapper.insert(user);
        return "插入成功";
    }

    //删(id)
    @SneakyThrows
    @RequestMapping("/delUser")
    public String delUser(Integer id){
        userMapper.deleteById(id);
        return "删除成功";
    }

    //改(id)
    @SneakyThrows
    @RequestMapping("/updateUser")
    public String updateUser(Integer id, String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        userMapper.updateById(user);
        return "修改成功";
    }

    //获取全部用户数据
    @SneakyThrows
    @RequestMapping("/getUser")
    public String getUser(){
        List<User> list = userMapper.selectList(null);
//        list.forEach(item->log.info(String.valueOf(item)));
        list.forEach(System.out::println);
        System.out.println(list);
        return "查询成功" + list;
    }

    //注册(id)
    @SneakyThrows
    @CrossOrigin
    @RequestMapping("/insert_User")
//    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    public String insert_User(String name,String password){
        System.out.println("姓名"+name);
        System.out.println("密码"+password);
        User user = new User();

        user.setName(name);
        user.setPassword(password);
        userMapper.insert(user);
        return "0";
    }

    //登录(id) 成功后返回id
    @SneakyThrows//检测报错
    @CrossOrigin//允许跨域传输
    @RequestMapping("/login_User")//设置路由
    @ResponseBody//将实体对象转换为json返回
    public Result login_User(String name, String password){
        List<User> list = userMapper.selectList(null);

        Result result = new Result();
        for (int i=0; i<list.toArray().length;i++){
            if (Objects.equals(name, list.get(i).getName())){
                if (Objects.equals(password, list.get(i).getPassword())){
                    System.out.println("登录时间: "+ LocalDateTime.now());
                    result.setError("2");
                    result.setId(list.get(i).getId());
                }
                else {
                    result.setError("1");
                }
                return result;
            }
        }
        result.setError("0");
        return result;
    }

    //获取用户信息
    @SneakyThrows//检测报错
    @CrossOrigin//允许跨域传输
    @RequestMapping("/select_User")//设置路由
    @ResponseBody//将实体对象转换为json返回
    public Result select_User(String id){
        List<User> list = userMapper.selectList(null);
        System.out.println("前端:"+id);

        Result result = new Result();
        for (int i=0; i<list.toArray().length;i++){
            System.out.println(list.get(i).getId().toString());
            if (Objects.equals(id, list.get(i).getId().toString())){
                result.setError("0");
                result.setName(list.get(i).getName());
                System.out.println("返回成功");
                return result;
            }
        }
        System.out.println("返回失败");
        result.setError("1");
        return result;
    }
}
