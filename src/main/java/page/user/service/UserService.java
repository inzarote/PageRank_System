package page.user.service;

import page.user.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
public interface UserService extends IService<User> {

}
