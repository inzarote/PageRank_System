package page.manor.service;

import page.manor.entity.Manor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
public interface ManorService extends IService<Manor> {

}
