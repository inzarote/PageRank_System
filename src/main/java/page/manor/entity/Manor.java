package page.manor.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Data
@TableName("manor")
@ApiModel(value = "Manor对象", description = "")
public class Manor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private Integer id;

    @TableField("number")
    private String number;

    @TableField("resident_n")
    private String resident_n;

    @TableField("level")
    private String level;

    @TableField("master")
    private String master;


}
