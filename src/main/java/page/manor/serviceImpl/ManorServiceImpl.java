package page.manor.serviceImpl;

import page.manor.entity.Manor;
import page.manor.mapper.ManorMapper;
import page.manor.service.ManorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Service
public class ManorServiceImpl extends ServiceImpl<ManorMapper, Manor> implements ManorService {

}
