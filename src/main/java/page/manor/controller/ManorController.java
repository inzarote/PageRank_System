package page.manor.controller;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import page.manor.entity.Manor;
import page.manor.mapper.ManorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yanyinan
 * @since 2022-04-03
 */
@Slf4j
@RestController
@RequestMapping("manor")
public class ManorController {

    @Autowired
    private ManorMapper manorMapper;

    //增(id)
    @SneakyThrows
    @RequestMapping("/insertManor")
    public String insertManor(String number){
        Manor manor = new Manor();
        manor.setNumber(number);
        manorMapper.insert(manor);
        return "插入成功";
    }

    //删(id)
    @SneakyThrows
    @RequestMapping("/delManor")
    public String delManor(Integer id){
        manorMapper.deleteById(id);
        return "删除成功";
    }

    //改(id)
    @SneakyThrows
    @RequestMapping("/updateManor")
    public String updateManor(Integer id, String number){
        Manor manor = new Manor();
        manor.setId(id);
        manor.setNumber(number);
        manorMapper.updateById(manor);
        return "修改成功";
    }

    //查（all）
    @SneakyThrows
    @RequestMapping("/getManor")
    public String getManor(){
        List<Manor> list = manorMapper.selectList(null);
//        list.forEach(item->log.info(String.valueOf(item)));
        list.forEach(System.out::println);
        System.out.println(list);
        return "查询成功" + list;
    }

}
